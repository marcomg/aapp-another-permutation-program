#!/usr/bin/php
<?php
// Define root
define('ROOT', getcwd());

// Load config
require(ROOT.'/includes/config.php');

// Load functions
require(ROOT.'/includes/various.function.php');

// Load config replacer
require(ROOT.'/includes/config_replacer.php');

// Load class to manager arguments
require(ROOT.'/includes/argsManager.class.php');
$argm = new argsManager($argv, array('-h', '--help'));

// Load gettext
require(ROOT.'/includes/gettext/gettext.inc');
T_setlocale(LC_MESSAGES, $locale);
T_bindtextdomain($domain, $locale_dir);
T_textdomain($domain);

// Welcome page
if(!$argm->isIssetWithAttributes('-w') and !$argm->isIssetAlone('-h') and !$argm->isIssetAlone('--help')){
    techo(T_('Welcome to aapp (aapp: another permutation program), if you need help you must insert -h argiment'));
    exit;
}
// Help page
elseif($argm->isIssetAlone('-h') or $argm->isIssetAlone('--help')){
    techo(T_('This is the help page of app!'));
    techo(T_('To use this app insert the arg -w with the world to permute.'));
    techo(T_('You can change the dictionary using -d dictionaryname.'));
    exit;
}

// Change default dictionary
if($argm->isIssetWithAttributes('-d')){
    $_ = $argm->getAttribute('-d');
    // If not isset an attribute I say I need it
    if($_ == false){
        techo(T_('You must insert the dictionary name or you must remove -d attribute.'));
        exit;
    }
    else{
        // If exist the dictionary
        if(file_exists(ROOT.'/includes/dictionaries/'.$_)){
            $dictionary = $_;
        }
        // If not exists print an error
        else{
            techo(T_('Error: the dictionary you required don\'t exists!'));
            exit;
        }
    }
}

// Get imput
$world = $argm->getAttribute('-w');

// Make permutations and insert it in an array
echo(T_('Permuting...'));
$strings = permutations($world);
techo(T_(' done.'));

// Load dictionary
echo(T_('Loadin dictionary...'));
$tmp = file(ROOT.'/includes/dictionaries/'.$dictionary);
// Insert worlds in the dictionary
foreach($tmp as $tm){
    $dic[] = trim(substr($tm, 0, -1));
}
techo(T_(' done.'));

// Unset unused var
unset($tmp);

// Remove worlds wich are not in the dictionary
echo(T_('Removing worlds whitch aren\'t in the dictionary...'));
$result = array_intersect($strings, $dic);
$result = array_unique($result);
techo(T_(' done.'));

// Print result
echo(T_('Worlds found: '));
if(empty($result)){
    techo(T_('none and the world you insert isn\' in the dictionary'));
}
else{
    techo(implode(', ', $result));
}
?>