��          �      �       0     1  0   8     i     ~  2   �     �  <   �  a        {  6   �  D   �  4     �  ;     �  3   �          (  3   8  $   l  `   �  e   �     X  9   i  D   �  :   �                              	       
                      done. Error: the dictionary you required don't exists! Loadin dictionary... Permuting... Removing worlds whitch aren't in the dictionary... This is the help page of app! To use this app insert the arg -w with the world to permute. Welcome to aapp (aapp: another permutation program), if you need help you must insert -h argiment Worlds found:  You can change the dictionary using -d dictionaryname. You must insert the dictionary name or you must remove -d attribute. none and the world you insert isn' in the dictionary Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-05-18 18:06+0200
PO-Revision-Date: 2013-05-18 18:10+0200
Last-Translator: Marco Guerrini <mmarcogguerrini@cryptolab.net>
Language-Team: Italian <kde-i18n-it@kde.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.4
  fatto. Errore: il dizionario che hai richiesto non esiste! Caricamento dizionario... Permutazione... Rimuovendo le parole che non sono nel dizionario... Questa è la pagina di aiuto di app! Per usare questa applicazione devi inserire come argomento -w seguito dalla parola da permutare. Benvenuto in aapp (aapp: another permutation pregram), se ti serve aiuto devi inserire l'argomento -h Parole trovate:  Puoi cambiare il dizionario usando -d nome_del_dizionario Devi inserire il nome del dizionario oppure rimuovere l'attributo -d nessuna a la parola che hai inserito non è nel dizionario 