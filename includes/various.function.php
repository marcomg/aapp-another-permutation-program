<?php
// Function for permutations
function permutations($buf){
    $buf = str_split($buf);
    $res = implode("",$buf);
    //echo("$res<br />");
    $return[] = $res;
    $string_length = count($buf);
    for($i=0;$i<$string_length;$i++){
        $p[$i]=0;
    }
    $i=1;
    while($i < $string_length){
        if ($p[$i] < $i) {
            $j = $i % 2 * $p[$i];
            $tmp=$buf[$j];
            $buf[$j]=$buf[$i];
            $buf[$i]=$tmp;
            $res = implode("",$buf);
            //echo("$res<br />");
            $return[] = $res;
            $p[$i]++;
            $i=1;
        }
        else {
            $p[$i] = 0;
            $i++;
        }
    }
    return $return;
}

// Function for terminal echo
function techo($input=null){
    if(!empty($input)){
        echo(iconv('ISO-8859-1', 'UTF-8', $input));
        echo(PHP_EOL);
    }
}

// Function return language
function system_language(){
    if(empty($_SERVER['LANG'])){
        return false;
    }
    else{
        $return = explode('.', $_SERVER['LANG']);
        return $return[0];
    }
}
?>