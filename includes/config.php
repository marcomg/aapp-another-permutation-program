<?php
// Gettext config
$locale_dir = ROOT.'/includes/local';
$supported_locales = array('en_US', 'it_IT'); // Lingue supportate
$encoding = 'UTF-8'; // Codifica
$locale = 'en_US'; // Lingua predefinita
$domain = 'messages'; // Dominio

// Dictionary config
$dictionary = 'it_IT.txt';

// Memory limit
ini_set('memory_limit','2G');
?>