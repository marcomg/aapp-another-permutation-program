<?php
/*
*  References:
*  This class is used to manager arguments in php-cli.
*  You have to *istenziare* the class in this way:
*  $argm = new argsManager($argv, array('-h', '--help'));
*  The first argument is not mutable and is an array where there ara all
*  arguments.
*  The second is an array with arguments that haven't a value, but are boolean.
*  
*/

/*
*  Author: Marco Guerrini <marcomg@cryptolab.net>
*  File: argsManager.class.php
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
class argsManager{
    
    function __construct($args, $aloneArgs = array('-h', '--help')){
        if(!is_array($args)){
            die('Error, $args must have been an array');
        }
        if(!is_array($aloneArgs)){
            die('Error, $aloneArgs must have been an array');
        }
        
        unset($args[0]);
        
        $i = 1;
        $alone = array(0 => null);
        $arguments = array(0 => null);
        $attributes = array(0 => null);
        
        foreach($args as $arg){
            // If must be alone
            if(in_array($arg, $aloneArgs)){
                $alone[] = $arg;
            }
            // If not alone
            else{
                if($this->isOdd($i)){
                    if(substr($arg, 0, 1) !== '-'){
                        die('Fatal error in class argsManager: Error parsing arguments. Only one attributa an arg can have and his first char must be \'-\'');
                    }
                    $arguments[] = $arg;
                }
                else{
                    $attributes[] = $arg;
                }
                $i++;
            }
        }
        if(empty($alone))
            $alone = array();
        if(empty($arguments))
            $arguments = array();
        if(empty($attributes))
            $attributes = array();
        
        $this->boolean_args = $alone;
        $this->args = $arguments;
        $this->attributes = $attributes;
        
        unset($i, $arguments, $attributes, $alone, $arg, $aloneArgs);
    }
    
    function isIssetAlone($arg){
        if(in_array($arg, $this->boolean_args) == false)
            return false;
        else
            return true;
    }
    
    function isIssetWithAttributes($arg){
        if(in_array($arg, $this->args) == false)
            return false;
        else
            return true;
    }
    
    function isIsset($arg){
        if($this->isIssetAlone($arg) or $this->isIssetWithAttributes($arg))
            return true;
        else
            return false;
    }
    
    function isIssetAttribute($arg){
        $_ = array_search($arg, $this->args);
        if($_ == false)
            return false;
        else
            return true;
    }
    
    function getAttribute($arg){
        $_ = array_search($arg, $this->args);
        if($_ == false)
            return false;
        else{
            if(empty($this->attributes[$_]))
                return false;
            else
                return $this->attributes[$_];
        }
    }
    
    private function isEven($num){
        if($num % 2 == 0)
            return true;
        else
            return false;
    }
    
    private function isOdd($num){
        if($num % 2 !== 0)
            return true;
        else
            return false;
    }
}

?>
